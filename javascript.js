let button2 = document.getElementById('button2')
let button3 = document.getElementById('button3')
let box = document.getElementById('box')


$("#button1").click(function() {
    $("#box").animate({
        width:"+=35",
        height:"+=35",
    })
});

//function turnBlue(DomID){
//  DomID.style.backgroundColor = "blue";   
//}

function turnBlue() {
    box.style.backgroundColor = "blue";
};

function fade() {
    let opacity = box.style.opacity;
    box.style.opacity = opacity ? (parseFloat(opacity) - 0.1) : 0.9;
};

$("#button4").click(function() {
    $("#box").animate({
        width:"150",
        height:"150",
        opacity:"1",
    })
    $("#box").css("background-color", "orange");
});

// button2.addEventListener('click', turnBlue(box));

button2.addEventListener('click', turnBlue);
button3.addEventListener('click', fade);